# Slovenija za odprto kodo

Javno sporočilo za spodbuditev prehoda obstoječe programske infastrukture javnega sektorja iz lastniške programske opreme na Linux operacijske sisteme in odprtokodno programsko opremo, kot je pisarniški paket LibreOffice. Vsebino lahko pomaga dopolnjevati vsak! Odprite meni *Issues* in dodajte predlog za spremembo ali dodajanje vsebine.

#### Trenutno stanje in problematika:

- Velika večina državnih organizacij, podjetij, občin itd. uporablja nepreverljivo programsko opremo, v glavnem ameriške korporacije Microsoft.
- Windows: zaprtokoden, drage licence za uporabo, telemetrija – sledenje, koriščenje podatkov za trženje oglasov...
- Office: zaprtokoden, drage licence, ne-odprti standardi...
- Starejši računalniki nekompatibilni s strmo zahtevnejšimi operacijskimi sistemi Windows

## Prehod na Linux in odprto kodo

#### Kaj to pomeni za ekonomijo:

- Nova delovna mesta in izobraževanja v podjetjih, ki se ukvarjajo z vzdrževanjem sistemov. 
- Zaposlitev lokalnih razvijalcev in vzdrževalcev.
- Denar za Microsoftove licence bi lahko namenili za ustrezno izobraževanje osebja
- Več-milijonski prihranki od nakupov licenc programske opreme Windows + Office, ki ne bi bila več potrebna, in bi se ju zamenjalo z ustrezno Linux distribucijo in pisarniškimi paketi LibreOffice, ki bi se lahko prilagodili na zelo specifične zahteve javnega sektorja.

#### Prednosti:

- »digitalna svoboda in neodvisnost, transparentnost in participativnost«
- Brezplačnost
- Vedno naraščajoča uporabljivost
- Vsak del operacijskega sistema in programske opreme preverljiv s strani organov, ki jih uporabljajo v svojih organizacijah.
- Boljša odpornost na razne računalniške viruse,ki za linux praktično ne predstavljajo težave
- Stroškov, razen IT-podpore ni.
- Stabilnost
- Deluje veliko hitreje na starejši strojni opremi, odvisno od distribucije Linux, lahko starejše računalnike s takimi sistemi "oživite", da delujejo kot novi.
- Kompatibilnost s širokim naborom nove in stare strojne opreme, kar zmanjša nabavne stroške nove strojne opreme
- Nadgradnja obstoječih delovnih postaj v JU na operacijske sisteme Linux bi pomenila naložbo v prihodnost za večjo neodvisnost od zaprte programske opreme tujih držav izven EU, kar pomeni, da bi celotna izvorna koda sistemov Linux bila prosto dostopna in bi jo lahko ustrezne vzdrževalske službe izboljševale.

#### Povezave:

- Podprtost programov za Linux v SLO https://wiki.lugos.si/projekti:podprtost-linuxa
- Evropsko gibanje fsfe, ki spodbuja, da bi bila programska koda, plačana z javnim denarjem tudi odprta: https://publiccode.eu 

#### Dosedanji pritiski za spremembe:

- Na-prostem.si: Prosto programje za digitalno vključenost NVO in uporabnikov: 
https://www.informacijska-druzba.org/2022/01/25/na-prostem-si-prosto-programje-za-digitalno-vkljucenost-nvo-in-uporabnikov/
- Linux in odprta koda za celotno javno upravo: 
https://predlagam.vladi.si/predlog/5719/ 
- Odprtokodna programska oprema: 
https://predlagam.vladi.si/predlog/46 
- Pobuda proti lastniški programski opremi: 
https://www.stopbirokraciji.gov.si/pobuda/?st=303 
- Na Linux bi lahko prešlo vsaj 90% zaposlenih v javni upravi:
http://www.lugos.si/novice/na-linux-bi-lahko-preslo-vsaj-90-odstotkov-zaposlenih-v-javni-upravi
- Uporaba odprtokodne programske opreme v javni upravi:
https://predlagam.vladi.si/predlog/3611 
- Znotraj mreže NVO-VID obstaja med drugimi tudi delovna skupina za FOSS, ki jo vodi LUGOS. Zapisniki in druge skupine so na voljo na: https://www.informacijska-druzba.org/delovne-skupine/

#### Razni dokumenti:

- Študija uvajanja odprtokodne programske opreme (okpo) na delovnih postajah v javni upravi: http://www.arhiv.mju.gov.si/fileadmin/mju.gov.si/pageuploads/mju_dokumenti/pdf/STUDIJA_ODPRTA_KODA.pdf
- Poročilo evropske komisije glede uporabe odprte kode:
https://digital-strategy.ec.europa.eu/en/library/study-about-impact-open-source-software-and-hardware-technological-independence-competitiveness-and
- Diplomsko delo Odprti standardi in odprtokodne rešitve
v javni upravi, Boris Upelj: http://dk.fdv.uni-lj.si/diplomska/pdfs/upelj-boris.pdf


#### Ostale organizacije:

- Na Prostem Si: https://na-prostem.si/
- Center odprte kode: https://www.coks.si/ 
- Društvo uporabnikov Linuxa v sloveniji, Lugos: http://www.lugos.si/drustvo

#### Primeri iz tujine:

- Nemška zvezna dežela Schleswig-Holstein bo naredila prehod na Linux s 25000 računalniki JU.
[SloTech](https://slo-tech.com/novice/t793484), [Računalniške Novice](https://racunalniske-novice.com/nemska-zvezna-dezela-bo-v-celoti-zamenjala-windows-za-linux/)
- LiMux (Minhen, 15000 računalnikov iz JU)
https://en.wikipedia.org/wiki/LiMux
- Turčija naredila prehod in prihranila že čez 1 milijon evrov. [FossPost](https://fosspost.org/turkish-municipality-migration-pardus-linux-opensource/)
- Seznam posvojiteljev Linuxa
https://en.wikipedia.org/wiki/List_of_Linux_adopters
- Zakaj je odvisnost Evrope od Microsofta veliko varnostno tveganje
https://www.investigate-europe.eu/en/2017/why-europes-dependency-on-microsoft-is-a-huge-security-risk/

#### Video vsebina:

- Na-prostem.si predstavitveni video: https://youtu.be/SlY_HDUpIuQ/
- Microsoft-Software: Safe for Europe? (Full Documentary, 2018)
https://www.youtube.com/watch?v=duaYLW7LQvg

#### Dodatno:

*Zakaj NE novejšemu Windows 11:*

- Najnovejši Windows 11 je predstavil še dodatne omejitve, kjer bo še bolj strikten nabor kompatibilne strojne opreme procesorjev in matičnih plošč, zaradi česar bi kakršnekoli nadgradnje operacijskih sistemov na omenjenega predstavljale tudi velike stroške nabave ter menjave strojne opreme
- Grafično zahtevnejši sistem, ki pri novejših različicah še bolj obremenjuje strojno opremo in ustvarja občutek počasnosti
- Potrebuje Grafični procesor, kompatibilen z najnovejšim (Microsoftovim) DirectX 12, in gonilnikom WDDM 2.0
- Potrebuje strojno opremo/modul TPM 2.0
- Glede na dosedanje škandale Microsoftovega zbiranja podatkov uporabnikov in profiliranja, lahko z gotovostjo predvidevamo, da bo omenjeno le še naprednejše in bo z novejšimi različicami Windows, poseg v uporabnikovo zasebnost še globlji
- JU ostane odvisna od Microsoft programske opreme, kar pomeni prilagajanje programski opremi in ne obratno
